#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright 2018 TUBITAK/UEKAE
# Licensed under the GNU General Public License, version 2.
# See the file http://www.gnu.org/copyleft/gpl.txt.

from pisi.actionsapi import autotools
from pisi.actionsapi import pisitools
from pisi.actionsapi import shelltools
from pisi.actionsapi import get

# WorkDir = ""
# NoStrip = "/"

def build():
    shelltools.system("cargo build --release")

def install():
    pisitools.dobin("target/release/geckodriver", "/usr/bin")
    
    pisitools.dosym("/usr/bin/geckodriver", "/usr/bin/wires")


from comar.service import *

serviceType = "local"
serviceConf = "ly"
serviceDefault = "conditional"

serviceDesc = _({"en": "Ly",
                 "tr": "Ly"})

@synchronized
def start():
    startService(command="/usr/sbin/ly",
                 args = config.get("args", "destroy"),
                 donotify=True)

@synchronized
def stop():
    stopService(command="/usr/sbin/ly",
                donotify=True)

def ready():
    import os
    status = is_on()
    if status == "on" or (status == "conditional" and os.path.exists("/sys/coffee/ready")):
        start()

def status():
    return checkDaemon("/var/run/ly.pid")

